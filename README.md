# Ud4_Ejemplo2
_Ejemplo 2 de la Unidad 4._ 

Vamos a modificar [Ud4_Ejemplo1](https://bitbucket.org/bitbucketcarlos/ud4_ejemplo1) para que al pulsar un elemento de la lista
aparezca un mensaje por pantalla con el nombre y aparición del vehículo.

Los cambios que deberemos realizar son los siguientes (los ficheros _activity_main.xml_, _elementos_lista.xml_ y _Vehiculo.kt_ son
idénticos al _Ud4_Ejemplo1_):

## _VehiculoAdapter.kt_

En esta clase es donde creamos el _onClickListener_ y lo asociamos a nuestro _ViewHolder_ creado.

Para ello debemos crear un atributo de tipo _onClickListener_:

```java
class VehiculoAdapter (private val lista: ArrayList<Vehiculo>): RecyclerView.Adapter<VehiculoAdapter.MiViewHolder>() {

    private var listener:View.OnClickListener? = null
    ...
```
Asignarlo al _ViewHolder_ en el método _onCreateViewHolder_:
```java
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        view.setOnClickListener(listener)

        return MiViewHolder(view)
    }
```
Y para poder asignar posteriormente el _onCLickListener_ al adaptador que usaremos en la clase _MainActivity_, creamos el 
método _setOnItemClickListener_ que asiganrá el _onClickListener_ pasado al de la clase:
```java
    fun setOnItemClickListerner(onClickListener: View.OnClickListener){
        listener = onClickListener
    }
```

## _MainActivity.kt_

En la clase principal deberemos añadir la asignación del _onclickListener_ al _adapter_ e implementar el mensaje a mostrar
por pantalla al pulsar el elemento:
```java
...
        // Asignamos el evento creado en VehiculoAdapter
        adapter.setOnItemClickListerner {
            val vehiculo = vehiculos.get(recycler.getChildAdapterPosition(it))
            Toast.makeText(this, vehiculo.nombre + "\n" + vehiculo.aparicion, Toast.LENGTH_LONG).show()
        }

        // Asignamos el adapter al RecyclerView
        recycler.adapter = adapter
    }
}
```