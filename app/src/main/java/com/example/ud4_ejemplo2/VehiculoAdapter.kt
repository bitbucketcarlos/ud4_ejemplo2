package com.example.ud4_ejemplo2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class VehiculoAdapter (private val lista: ArrayList<Vehiculo>): RecyclerView.Adapter<VehiculoAdapter.MiViewHolder>() {

    private var listener:View.OnClickListener? = null

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombretextView: TextView
        val apariciontextView: TextView

        init {
            nombretextView = view.findViewById(R.id.nombreTextView)
            apariciontextView = view.findViewById(R.id.aparicionTextView)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_lista, viewGroup, false)

        view.setOnClickListener(listener)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el vehículo de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        viewHolder.nombretextView.text = lista[position].nombre
        viewHolder.apariciontextView.text = lista[position].aparicion
    }

    // Devolvemos el tamaño de la lista de Vehículos
    override fun getItemCount() = lista.size

    // Asignamos el Click Listener al listener de la clase
    fun setOnItemClickListerner(onClickListener: View.OnClickListener){
        listener = onClickListener
    }
}