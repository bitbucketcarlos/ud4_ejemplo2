package com.example.ud4_ejemplo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ud4_ejemplo2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Creamos el array de vehiculos.
        val vehiculos = ArrayList<Vehiculo>()

        vehiculos.add(Vehiculo("Ecto1", "Los cazafantasmas"))
        vehiculos.add(Vehiculo("DeLorean", "Regreso al futuro"))
        vehiculos.add(Vehiculo("Kitt", "El coche fantástico"))
        vehiculos.add(Vehiculo("Halcón Milenario", "Star Wars"))
        vehiculos.add(Vehiculo("Planet Express", "Futurama"))
        vehiculos.add(Vehiculo("TARDIS", "Doctor Who"))
        vehiculos.add(Vehiculo("USS Enterprise", "Star Trek"))
        vehiculos.add(Vehiculo("Nabucodonosor", "Matrix"))
        vehiculos.add(Vehiculo("Odiseus", "Ulises 31"))
        vehiculos.add(Vehiculo("Nostromo", "Alien"))

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        val recycler = binding.recyclerView

        recycler.setHasFixedSize(true)

        // Añadimos la línea de separación de elementos de la lista
        // 0 para horizontal y 1 para vertical
        recycler.addItemDecoration(DividerItemDecoration(this, 1))

        // Asignamos un LinearLayout que contendrá cada elemento del RecyclerView
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        val adapter = VehiculoAdapter(vehiculos)

        // Asignamos el evento creado en VehiculoAdapter
        adapter.setOnItemClickListerner {
            val vehiculo = vehiculos.get(recycler.getChildAdapterPosition(it))
            Toast.makeText(this, vehiculo.nombre + "\n" + vehiculo.aparicion, Toast.LENGTH_LONG).show()
        }

        // Asignamos el adapter al RecyclerView
        recycler.adapter = adapter
    }
}